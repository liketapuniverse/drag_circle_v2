//
//  DragCircle.swift
//  drag-circle-redo
//
//  Created by Fourth Dev on 10/11/2021.
//

import UIKit

protocol DragCircleDelegate: AnyObject {
    func percentChange(_ percent: Double)
}

class DragCircle: UIView {

    let smallRadius: CGFloat = 26
    var percent: CGFloat = 0.28
    let viewBlue = UIColor(red: 80, green: 121, blue: 240)
    let blueLineWidth: CGFloat = 4
    let dragRadius: CGFloat = 15
    let dragCircleLineWidth: CGFloat = 5
    
    let bigCircle = CAShapeLayer()
    let smallCircle = CAShapeLayer()
    let dragCircle = UIView()
    let text = CenterTextLayer()
    let middleCircle = CAShapeLayer()
    let circleForBorder = CAShapeLayer()
    
    var didLoad = false
    weak var delegate: DragCircleDelegate?
    
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        buildBorder(percent)
        buildBigCircle()
        buildMiddleCircle(percent)
        buildSmallCircle()
        buildText()
        buildDragCircle()
        didLoad = true
    }
    
    func buildDragCircle() {
        let radian: Double = (Double.pi)*2*percent
        dragCircle.frame = CGRect(origin: .zero, size: CGSize(width: dragRadius*2, height: dragRadius*2))
        dragCircle.backgroundColor = .white
        dragCircle.layer.cornerRadius = dragRadius
        dragCircle.layer.borderColor = viewBlue.cgColor
        dragCircle.layer.borderWidth = dragCircleLineWidth
        dragCircle.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
        
        let distanceToCenter = bounds.midX - dragRadius
        let newX = distanceToCenter*sin(radian) + bounds.midX
        let newY = bounds.midY - distanceToCenter*cos(radian)
        dragCircle.center = CGPoint(x: newX, y: newY)
        addSubview(dragCircle)
    }
    
    func buildText() {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let textWidth: CGFloat = 60
        text.frame = CGRect(x: circleCenter.x - textWidth/2, y: circleCenter.y - textWidth/2, width: textWidth, height: textWidth)
        text.string = "\(Int(percent*100))%"
        text.foregroundColor = viewBlue.cgColor
        text.font = UIFont.systemFont(ofSize: 25)
        text.fontSize = 16
        text.alignmentMode = .center
        layer.addSublayer(text)
    }
    
    func buildBigCircle() {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let bigRadius = bounds.midX - dragRadius
        bigCircle.path = UIBezierPath(arcCenter: circleCenter, radius: bigRadius, startAngle: 0, endAngle: .pi*2, clockwise: true).cgPath
        bigCircle.fillColor = UIColor(red: 237, green: 241, blue: 253).cgColor
        layer.addSublayer(bigCircle)
    }
    
    func buildBorder(_ percent: Double) {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let bigRadius = bounds.midX - dragRadius
        circleForBorder.path = UIBezierPath(arcCenter: circleCenter, radius: bigRadius, startAngle: -.pi/2, endAngle: -.pi/2 + .pi*2*percent, clockwise: true).cgPath
        circleForBorder.fillColor = nil
        circleForBorder.strokeColor = viewBlue.cgColor
        circleForBorder.lineWidth = blueLineWidth
        circleForBorder.lineCap = .round
        layer.addSublayer(circleForBorder)
    }
    
    func buildSmallCircle() {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        smallCircle.path = UIBezierPath(arcCenter: circleCenter, radius: smallRadius, startAngle: 0, endAngle: .pi*2, clockwise: true).cgPath
        smallCircle.fillColor = UIColor.white.cgColor
        layer.addSublayer(smallCircle)
    }
    
    func buildMiddleCircle(_ percent: Double) {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let middleRadius = smallRadius + (bounds.midX - smallRadius - dragRadius - blueLineWidth/2)*percent
        middleCircle.path = UIBezierPath(arcCenter: circleCenter, radius: middleRadius, startAngle: 0, endAngle: .pi*2, clockwise: true).cgPath
        middleCircle.fillColor = viewBlue.cgColor
        middleCircle.shadowColor = viewBlue.cgColor
        middleCircle.shadowRadius = 20
        middleCircle.shadowOpacity = 0.5
        middleCircle.shadowOffset = CGSize(width: 0, height: 2)
        layer.addSublayer(middleCircle)
    }
    
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        if g.state == .changed {
            let point = g.location(in: self)
            let dX =  point.x - bounds.midX
            let dY = bounds.midY - point.y
            var radian = atan2(dX, dY)
            if (radian < 0) { radian = .pi*2 + radian}
            let distanceToCenter = bounds.midX - dragRadius
            let newX = distanceToCenter*sin(radian) + bounds.midX
            let newY = bounds.midY - distanceToCenter*cos(radian)
            dragCircle.center = CGPoint(x: newX, y: newY)
            let newPercent = radian/(2*Double.pi)
            if abs(newPercent - percent) > 0.5 {
                percent = percent < 0.5 ? 0 : 1
                dragCircle.center = CGPoint(x: bounds.midX, y: dragRadius)
            } else {
                percent = newPercent
            }
            buildBorder(percent)
            buildMiddleCircle(percent)
            buildSmallCircle()
            buildText()
            bringSubviewToFront(dragCircle)
            if let d = delegate {
                d.percentChange(percent)
            }
        }
    }
}

class CenterTextLayer: CATextLayer {
    override func draw(in ctx: CGContext) {
        let height = self.bounds.height
        let fontSize = self.fontSize
        let yDiff = (height - fontSize)/2
        ctx.saveGState()
        ctx.translateBy(x: 0, y: yDiff)
        super.draw(in: ctx)
        ctx.restoreGState()
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}
