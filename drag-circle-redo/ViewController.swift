//
//  ViewController.swift
//  drag-circle-redo
//
//  Created by Fourth Dev on 10/11/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dragCircle: DragCircle!
    override func viewDidLoad() {
        super.viewDidLoad()
        dragCircle.percent = 0.1
        dragCircle.delegate = self
    }
}

extension ViewController: DragCircleDelegate {
    func percentChange(_ percent: Double) {
        print("received percent \(percent)")
    }
}
